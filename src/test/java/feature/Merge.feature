Feature: Login to LeafTaps 

Scenario Outline: Merge Lead 
	And Enter the UserName as <userName> 
	And Enter the Password as <password> 
	When Click on the Login Button 
	And Click CRM/SFA 
	And Click Leads 
	And Merge Leads 
	When Click on Icon Near From Lead And Move to New Window as <newWindowIndexFrom> 
	And Enter FromLeadID as <fromLeadID> 
	And Click Find Leads Button 
	When Click on First Leading Lead and SWitch back to Primary Window as <primaryWindowIndexFrom> 
	When Click on Icon Near To Lead And Move to New Window as <newWindowIndexTo> 
	And Enter toLeadID as <toLeadID> 
	And Click Find Leads Button
	When Click on First Leading Lead and SWitch back to Primary Window as <primaryWindowIndexTo> 
	Then Click Merge and Accept Alert 
	And Click Find Leads 
	And Enter FromLeadID as <searchLeadID> 
	And Find Leads Button 
	And Verify Error Msg 
	
	Examples: 
	|userName|password|newWindowIndexFrom|fromLeadID|primaryWindowIndexFrom|newWindowIndexTo|toLeadID|primaryWindowIndexTo|searchLeadID|
	|DemoSalesManager|crmsfa|1|10055|0|1|10057|0|10055|