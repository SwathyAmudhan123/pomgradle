Feature: Login to LeafTaps


Scenario Outline: Edit Lead
And Enter the UserName as <userName>
And Enter the Password as <password>
When Click on the Login Button
And Click CRM/SFA
And Click Leads
And Click Find Leads
And Enter FirstName for Search as <firstName>
And Find Leads Button
When Click on First Leading Lead
Then Verify the Title Of the Page
And Click Edit
And Change the Company Name as <companyName>
When Click Update
Then Confirm the Changed Name Appears as <companyName>



Examples:
|userName|password|firstName|companyName|
|DemoSalesManager|crmsfa|Ji|Accenture|