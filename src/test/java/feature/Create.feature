Feature: Login to LeafTaps

#Background:
#Given Open The Browser
#And Max the Browser
#And Set the TimeOut
#And Launch The URL

Scenario Outline: Create Lead
And Enter the UserName as <userName>
And Enter the Password as <password>
When Click on the Login Button
And Click CRM/SFA
And Click Leads
And Click Create Lead
And Enter CompanyName as <companyName>
And Enter FirstName as <firstName>
And Enter LastName as <lastName>
When Click the SubmitButton
Then Verify the FirstName

Examples:
|userName|password|companyName|firstName|lastName|
|DemoSalesManager|crmsfa|Accenture|swathy|bala|

Scenario: UserData Login
And Enter the UserName as DemoCSR
And Enter the Password as crmsfa
When Click on the Login Button

