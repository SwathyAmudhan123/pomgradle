Feature: Login to LeafTaps


Scenario Outline: Duplicate Lead
And Enter the UserName as <userName>
And Enter the Password as <password>
When Click on the Login Button
And Click CRM/SFA
And Click Leads
And Click Find Leads
And Click on Email 
And Enter EmailID as <emailID>
And Click Find Leads Button
And Capture Name and Click First resulting Lead
And Click Duplicate Lead
And Verify the Section Page title
And Click Create Lead in Duplicate Lead Page
And Confirm the duplicated lead name is same as captured name


Examples:
|userName|password|emailID|
|DemoSalesManager|crmsfa|demo@demolead1.com|
