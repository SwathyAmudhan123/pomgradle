Feature: Login to LeafTaps


Scenario Outline: Delete Lead
And Enter the UserName as <userName>
And Enter the Password as <password>
When Click on the Login Button
And Click CRM/SFA
And Click Leads
And Click Find Leads
And Click on Phone
When Enter the Phone number as <phoneNumber>
And Click Find Leads Button
And Capture LeadId and Click the First Resulting Lead
And Click Delete
And Click Find Leads
And Enter the Capture LeadID
And Click Find Leads
And Verify Error Msg

Examples:
|userName|password|phoneNumber|
|DemoSalesManager|crmsfa|681-3029|