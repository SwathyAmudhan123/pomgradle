/*
 * package steps;
 * 
 * import java.util.concurrent.TimeUnit;
 * 
 * import org.openqa.selenium.chrome.ChromeDriver; import
 * cucumber.api.java.en.Given; import cucumber.api.java.en.Then; import
 * cucumber.api.java.en.When;
 * 
 * public class CreateLead { ChromeDriver driver; String userDefinedFirstName;
 * String displayedFirstName;
 * 
 * @Given("Open The Browser") public void openTheBrowser() {
 * System.setProperty("Webdriver.Chromedriver", "./drivers/chromedriver.exe");
 * driver = new ChromeDriver(); }
 * 
 * @Given("Max the Browser") public void maxTheBrowser() {
 * driver.manage().window().maximize(); }
 * 
 * @Given("Set the TimeOut") public void setTheTimeOut() {
 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); }
 * 
 * @Given("Launch The URL") public void launchTheURL() {
 * driver.get("http://leaftaps.com/opentaps/"); }
 * 
 * @Given("Enter the UserName as (.*)") public void
 * enterTheUserNameAsDemoSalesManager(String data) {
 * driver.findElementById("username").sendKeys(data); }
 * 
 * @Given("Enter the Password as (.*)") public void
 * enterThePasswordAsCrmsfa(String data) {
 * driver.findElementById("password").sendKeys(data); }
 * 
 * @When("Click on the Login Button") public void clickOnTheLoginButton() {
 * driver.findElementByClassName("decorativeSubmit").click(); }
 * 
 * @When("Click CRM\\/SFA") public void clickCRMSFA() {
 * driver.findElementByLinkText("CRM/SFA").click(); }
 * 
 * @When("Click Leads") public void clickLeads() {
 * driver.findElementByLinkText("Leads").click(); }
 * 
 * @When("Click Create Lead") public void clickCreateLead() {
 * driver.findElementByLinkText("Create Lead").click(); }
 * 
 * @When("Enter CompanyName as (.*)") public void
 * enterCompanyNameAsAccenture(String data) {
 * driver.findElementById("createLeadForm_companyName").sendKeys(data); }
 * 
 * @When("Enter FirstName as (.*)") public void enterFirstNameAsSwathy(String
 * data) { driver.findElementById("createLeadForm_firstName").sendKeys(data);
 * userDefinedFirstName =
 * driver.findElementById("createLeadForm_firstName").getAttribute("value"); }
 * 
 * @When("Enter LastName as (.*)") public void enterLastNameAsBala(String data)
 * { driver.findElementById("createLeadForm_lastName").sendKeys(data); }
 * 
 * @When("Click the SubmitButton") public void clickTheSubmitButton() {
 * driver.findElementByClassName("smallSubmit").click(); }
 * 
 * @Then("Verify the FirstName") public void verifyTheFirstName() {
 * displayedFirstName =
 * driver.findElementById("viewLead_firstName_sp").getText(); if
 * (displayedFirstName.equals(userDefinedFirstName)) {
 * System.out.println("Verified the First name,both are same"); } else {
 * System.out.println("Both are not same"); } } }
 */