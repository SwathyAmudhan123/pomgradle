package runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src//test//java//feature//Edit.feature", glue = { "steps",
		"pages" }, monochrome = true /* ,dryRun = true, snippets = SnippetType.CAMELCASE */)
public class EditLeadRunner {

}
