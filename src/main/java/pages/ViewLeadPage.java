package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "viewLead_firstName_sp")
	WebElement elVerifyFirstName;
	@FindBy(id = "viewLead_companyName_sp")
	WebElement elVerifyCompanyName;
	@FindBy(linkText = "Edit")
	WebElement eleditLink;
	@FindBy(linkText = "Duplicate Lead")
	WebElement eleDuplicateLeadLink;
	@FindBy(linkText = "Delete")
	WebElement eleClickDeleteButton;

	@Then("Verify the FirstName")
	public ViewLeadPage verifyTheFirstName() {
		verifyExactText(elVerifyFirstName, CreateLeadUserDataPage.firstNameUserData);
		return this;
	}

	@When("Confirm the duplicated lead name is same as captured name")
	public ViewLeadPage duplicateNameVsCapturedName() {
		verifyExactText(elVerifyFirstName, FindLeadsPage.capturedFirstName);
		return this;
	}

	public ViewLeadPage verifyFirstName(String firstName) {
		verifyExactText(elVerifyFirstName, firstName);
		return this;
	}

	@Then("Confirm the Changed Name Appears as (.*)")
	public ViewLeadPage verifyCompanyName(String companyName) {
		verifyPartialText(elVerifyCompanyName, companyName);
		return this;
	}

	@When("Click Delete")
	public MyLeadsPage clickDeleteButton() {
		click(eleClickDeleteButton);
		return new MyLeadsPage();
	}

	@Then("Click Edit")
	public OpenTapsCRMPage EditLinkClick() {
		click(eleditLink);
		return new OpenTapsCRMPage();
	}

	@Then("Verify the Title Of the Page")
	public ViewLeadPage verifyTitleOfThePage() {
		String pageTitle = driver.getTitle();
		verifyTitle(pageTitle);
		return this;
	}

	@When("Click Duplicate Lead")
	public DuplicateLeadPage DuplicateLeadLinkClick() {
		click(eleDuplicateLeadLink);
		return new DuplicateLeadPage();
	}

}
