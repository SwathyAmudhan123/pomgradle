package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {

	static String capturedFirstName;
	static String capturedLeadID;

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@name='id']")
	WebElement eleFromLeadLeads;
	@FindBy(xpath = "//input[@name='id']")
	WebElement eleToLeadLeads;
	@FindBy(xpath = "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eleFirstLeadIdSearched;
	@FindBy(xpath = "(//input[@name='firstName'])[3]")
	WebElement eleFirstName;
	@FindBy(xpath = "(//button[text()='Find Leads'])")
	WebElement elFindLeadsButton;
	@FindBy(xpath = "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a")
	WebElement eleFirstNameSearched;
	@FindBy(xpath = "//span[text()='Email']")
	WebElement eleClickEmailID;
	@FindBy(name = "emailAddress")
	WebElement emailIDInput;
	@FindBy(xpath = "//span[text()='Phone']")
	WebElement elePhoneNoLink;
	@FindBy(xpath = "//input[@name='phoneNumber']")
	WebElement elePhoneNumber;
	@FindBy(xpath = "//input[@name='phoneAreaCode']")
	WebElement elePhoneAreaCode;
	@FindBy(xpath = "//input[@name='phoneCountryCode']")
	WebElement elePhoneCountryCode;
	@FindBy(xpath = "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eleLeadId;
	@FindBy(xpath = "//div[@class='x-paging-info']")
	WebElement errorMessage;

	@And("Enter FromLeadID as (.*)")
	public FindLeadsPage findFromLeads(String fromleadId) {
		System.out.println(fromleadId);
		type(eleFromLeadLeads, fromleadId);
		return this;
	}

	@And("Enter toLeadID as (.*)")
	public FindLeadsPage findToLeadLeads(String toleadId) {
		type(eleToLeadLeads, toleadId);
		return this;
	}

	@When("Find Leads Button")
	public FindLeadsPage clickFindLeadsButton() {
		click(elFindLeadsButton);
		return this;
	}

	@When("Click on First Leading Lead")
	public ViewLeadPage clickFindLeadsReturnViewPage() {
		click(eleLeadId);
		return new ViewLeadPage();
	}

	@When("Capture Name and Click First resulting Lead")
	public ViewLeadPage capturedFirstNameAndClickFindLeadsReturnPage() {
		capturedFirstName = getText(eleFirstNameSearched);
		click(eleFirstNameSearched);
		return new ViewLeadPage();
	}

	@When("Capture LeadId and Click the First Resulting Lead")
	public ViewLeadPage CapturedLeadIDAndclickFindLeadsReturnPage() {
		capturedLeadID = getText(eleLeadId);
		click(eleLeadId);
		return new ViewLeadPage();
	}

	@When("Click on First Leading Lead and SWitch back to Primary Window as (.*)")
	public MergeLeadsPage clickFirstFindLead(String index) {
		clickWithNoSnap(eleFirstLeadIdSearched);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		switchToWindow(Integer.parseInt(index));
		return new MergeLeadsPage();
	}

	@When("Enter FirstName for Search as (.*)")
	public FindLeadsPage enterFirstNameForSearch(String firstName) {
		type(eleFirstName, firstName);
		return this;
	}

	@When("Click Find Leads Button")
	public FindLeadsPage findLeadButton() {
		click(elFindLeadsButton);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	@When("Click on Email")
	public FindLeadsPage clickEmailID() {
		click(eleClickEmailID);
		return this;
	}

	@When("Click on Phone")
	public FindLeadsPage clickPhoneNo() {
		click(elePhoneNoLink);
		return this;
	}

	@When("Enter EmailID as (.*)")
	public FindLeadsPage emailIDInput(String emailID) {
		type(emailIDInput, emailID);
		return this;
	}

	@When("Enter the Phone number as (.*)")
	public FindLeadsPage phoneNumberInput(String phoneNumber) {
		elePhoneCountryCode.clear();
		elePhoneAreaCode.clear();
		type(elePhoneNumber, phoneNumber);
		return this;
	}

	@When("Enter the Capture LeadID")
	public FindLeadsPage putleadID() {
		type(eleFromLeadLeads, capturedLeadID);
		return this;
	}

	@When("Verify Error Msg")
	public FindLeadsPage verifyEleDisplayed() {
		verifyDisplayed(errorMessage);
		return this;
	}
}
