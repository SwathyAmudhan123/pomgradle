package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods {

	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@id='partyIdFrom']/following::a[1]")
	WebElement eleFromLead;
	@FindBy(xpath = "//input[@id='partyIdTo']/following::a[1]")
	WebElement eleToLead;
	@FindBy(linkText = "Merge")
	WebElement eleClickMergeLead;

	@When("Click on Icon Near From Lead And Move to New Window as (.*)")
	public FindLeadsPage fromLead(String index) {
		click(eleFromLead);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		switchToWindow(Integer.parseInt(index));
		return new FindLeadsPage();
	}

	@When("Click on Icon Near To Lead And Move to New Window as (.*)")
	public FindLeadsPage toLead(String index) {
		click(eleToLead);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		switchToWindow(Integer.parseInt(index));
		return new FindLeadsPage();
	}

	@Then("Click Merge and Accept Alert")
	public MyLeadsPage mergeLead() {
		clickWithNoSnap(eleClickMergeLead);
		acceptAlert();
		return new MyLeadsPage();
	}

}
