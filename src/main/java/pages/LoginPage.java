package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "username")
	WebElement eleuserName;
	@FindBy(id = "password")
	WebElement elePassword;
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit")
	WebElement eleLogin;
	@FindBy(how = How.XPATH, using = "//div[@id='errorDiv']/p[2]")
	WebElement eleErrMsg;

	@Given("Enter the UserName as (.*)")
	public LoginPage enterUserName(String data) {
		type(eleuserName, data);
		return this;
	}

	@Given("Enter the Password as (.*)")
	public LoginPage enterPassword(String data) {
		type(elePassword, data);
		return this;
	}

	@When("Click on the Login Button")
	public HomePage clickLogin() {
		click(eleLogin);
		return new HomePage();
	}

	public LoginPage clickLoginForFailure() {
		click(eleLogin);
		return this;
	}

	public LoginPage verifyErrorMsg(String data) {
		verifyPartialText(eleErrMsg, data);
		return this;
	}

}
