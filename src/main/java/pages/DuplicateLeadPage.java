package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods {
	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "sectionHeaderTitle_leads")
	WebElement eleSectionHeaderTitle;
	@FindBy(name = "submitButton")
	WebElement eleCreateLeadButton;

	@When("Click Create Lead in Duplicate Lead Page")
	public ViewLeadPage createLead() {
		click(eleCreateLeadButton);
		return new ViewLeadPage();
	}

	@When("Verify the Section Page title")
	public DuplicateLeadPage verifySectionHeader() {
		verifyPartialText(eleSectionHeaderTitle, "Duplicate Lead");
		return this;
	}

}
