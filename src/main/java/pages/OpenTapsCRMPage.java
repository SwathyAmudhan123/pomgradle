package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class OpenTapsCRMPage extends ProjectMethods {

	public OpenTapsCRMPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "updateLeadForm_companyName")
	WebElement elCompanyNameUpdate;
	@FindBy(name = "submitButton")
	WebElement elSubmitButton;

	@Then("Change the Company Name as (.*)")
	public OpenTapsCRMPage companyNameUpdate(String userCompanyNameUpdate) {
		elCompanyNameUpdate.clear();
		type(elCompanyNameUpdate, userCompanyNameUpdate);
		return this;
	}

	@When("Click Update")
	public ViewLeadPage submitButton() {
		click(elSubmitButton);
		return new ViewLeadPage();
	}
}
