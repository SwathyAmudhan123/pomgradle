package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods {

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText = "Create Lead")
	WebElement elCreateLead;
	@FindBy(linkText = "Merge Leads")
	WebElement elMergeLead;
	@FindBy(linkText = "Find Leads")
	WebElement elFindLeadsLink;

	@When("Click Create Lead")
	public CreateLeadUserDataPage ClickCreateLeads() {
		click(elCreateLead);
		return new CreateLeadUserDataPage();
	}

	@When("Merge Leads")
	public MergeLeadsPage clickMergeLeads() {
		click(elMergeLead);
		return new MergeLeadsPage();
	}

	@When("Click Find Leads")
	public FindLeadsPage clickFindLeadsLink() {
		click(elFindLeadsLink);
		return new FindLeadsPage();
	}

}
