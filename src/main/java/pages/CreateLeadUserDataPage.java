package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadUserDataPage extends ProjectMethods {

	public static String firstNameUserData;

	public CreateLeadUserDataPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "createLeadForm_companyName")
	WebElement elCompanyName;
	@FindBy(id = "createLeadForm_firstName")
	WebElement elFirstName;
	@FindBy(id = "createLeadForm_lastName")
	WebElement elLastName;
	@FindBy(className = "smallSubmit")
	WebElement elSubmitButton;

	@When("Enter CompanyName as (.*)")
	public CreateLeadUserDataPage companyName(String companyName) {
		type(elCompanyName, companyName);
		return this;
	}

	@When("Enter FirstName as (.*)")
	public CreateLeadUserDataPage firstName(String firstName) {
		type(elFirstName, firstName);
		firstNameUserData = firstName;
		return this;
	}

	@When("Enter LastName as (.*)")
	public CreateLeadUserDataPage lastName(String lastName) {
		type(elLastName, lastName);
		return this;
	}

	@When("Click the SubmitButton")
	public ViewLeadPage submitButtonClick() {
		click(elSubmitButton);
		return new ViewLeadPage();
	}
}
