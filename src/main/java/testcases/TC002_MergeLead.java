package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_MergeLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_MergeLead";
		testDescription = "MergeLead";
		authors = "swathy";
		category = "smoke";
		dataSheetName = "TC002_MergeLead";
		testNodes = "Leads";
	}

	@Test(dataProvider = "fetchData")
	public void MergeLead(String userName, String password, String FromLeadID, String toLeadID, String findFromID) {
		new LoginPage().enterUserName(userName).enterPassword(password).clickLogin().CRMLink().clickLeads()
				.clickMergeLeads().fromLead("1").findFromLeads(FromLeadID).clickFindLeadsButton()
				.clickFirstFindLead("0").toLead("1").findToLeadLeads(toLeadID).clickFindLeadsButton()
				.clickFirstFindLead("0").mergeLead().clickFindLeadsLink().findFromLeads(findFromID)
				.clickFindLeadsButton().verifyEleDisplayed();
	}
}
