package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DuplicateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_DuplicateLead";
		testDescription = "Edit the content in description";
		authors = "Swathy";
		category = "sanity";
		dataSheetName = "TC004_DuplicateLead";
		testNodes = "Leads";
	}

	@Test(dataProvider = "fetchData")
	public void duplicateLead(String userName, String password, String emailID) {
		new LoginPage().enterUserName(userName).enterPassword(password).clickLogin().CRMLink().clickLeads()
				.clickFindLeadsLink().emailIDInput(emailID).clickFindLeadsButton().capturedFirstNameAndClickFindLeadsReturnPage()
				.DuplicateLeadLinkClick().verifySectionHeader().createLead().duplicateNameVsCapturedName();

	}
}
