package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_EditLead";
		testDescription = "Edit the content in description";
		authors = "Swathy";
		category = "smoke";
		dataSheetName = "TC003_EditLead";
		testNodes = "Leads";
	}

	@Test(dataProvider = "fetchData")
	public void EditLead(String userName, String password, String firstName, String companyNameUpdate,
			String pageTitle) {
		new LoginPage().enterUserName(userName).enterPassword(password).clickLogin().CRMLink().clickLeads()
				.clickFindLeadsLink().enterFirstNameForSearch(firstName).findLeadButton()
				.capturedFirstNameAndClickFindLeadsReturnPage().verifyTitleOfThePage().EditLinkClick()
				.companyNameUpdate(companyNameUpdate).submitButton().verifyCompanyName(companyNameUpdate);
	}
}
