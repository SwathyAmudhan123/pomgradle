package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LoginAndLogout";
		testDescription = "LoginAndLogout";
		authors = "sarath";
		category = "smoke";
		dataSheetName = "TC001_CreateLead";
		testNodes = "Leads";
	}

	@Test(dataProvider = "fetchData")
	public void login(String userName, String password, String companyName, String firstName, String lastName) {
		new LoginPage().enterUserName(userName).enterPassword(password).clickLogin().CRMLink().clickLeads()
				.ClickCreateLeads().companyName(companyName).firstName(firstName).lastName(lastName).submitButtonClick()
				.verifyFirstName(firstName);
		System.out.println("Verified the First name" + firstName + "successfully");
	}
}
