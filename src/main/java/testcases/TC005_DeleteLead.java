package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC005_DeleteLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC005_DeleteLead";
		testDescription = "Delete the lead";
		authors = "Swathy";
		category = "sanity";
		dataSheetName = "TC005_DeleteLead";
		testNodes = "Leads";
	}

	@Test(dataProvider = "fetchData")
	public void deleteLead(String userName, String password, String phoneNumber) {
		new LoginPage().enterUserName(userName).enterPassword(password).clickLogin().CRMLink().clickLeads().clickFindLeadsLink()
				.clickPhoneNo().phoneNumberInput(phoneNumber).clickFindLeadsButton()
				.CapturedLeadIDAndclickFindLeadsReturnPage().clickDeleteButton().clickFindLeadsLink().putleadID().clickFindLeadsButton()
				.verifyEleDisplayed();
	}
}
